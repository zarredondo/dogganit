from __future__ import print_function

from os.path import join

from torch.autograd import Variable

import argparse
import os
import sys
import random
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
import torchvision.datasets as dset
import torchvision.transforms as transforms
import torchvision.utils as vutils
import numpy as np

root = os.path.realpath(os.path.join(os.path.dirname(__file__), ".."))

default_data_dir = join("data", "Images")
default_checkpoint_base_dir = join("checkpoints")

def paths(
        project_name,
        data_dir = default_data_dir,
        checkpoint_base = default_checkpoint_base_dir
    ): # type: (str, str, str) -> Tuple[str, str]
    chkpnt_dir = join(root, checkpoint_base, project_name)
    data_dir = join(root, data_dir)

    return chkpnt_dir, data_dir

def seed(s = None): # type: (Optional[int]) -> None
    """Set random seed for reproducibility.

    Unless given a seed, picks a random one.
    """
    if s is None:
        s = random.randint(1, 10000)

    print("Random Seed: ", s)

    random.seed(s)
    torch.manual_seed(s)

def make_dataloader(data_dir, image_size, batch_size, num_workers):
    dataset = dset.ImageFolder(root=data_dir,
                          transform=transforms.Compose([
                              transforms.Resize(image_size),
                              transforms.CenterCrop(image_size),
                              transforms.ToTensor(),
                              transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
                          ]))
    return torch.utils.data.DataLoader(dataset,
        batch_size=batch_size,
        shuffle=True,
        num_workers=num_workers
    )

def load_checkpoint(netG, netD, loss, fixed_noise, optimizerG, optimizerD, G_losses, D_losses, iters, filename, device):
    # Note: Input model & optimizer should be pre-defined.  This routine only updates their states.
    start_epoch = 0
    iters = 0
    if os.path.isfile(filename):
        print("=> loading checkpoint '{}'".format(filename))
        checkpoint = torch.load(filename, map_location=device)
        start_epoch = checkpoint['epoch']
        netG.load_state_dict(checkpoint['netG_state_dict'])
        netD.load_state_dict(checkpoint['netD_state_dict'])
        criterion = checkpoint['criterion']
        fixed_noise = checkpoint['fixed_noise']
        optimizerG.load_state_dict(checkpoint['optimizerG_state_dict'])
        optimizerD.load_state_dict(checkpoint['optimizerD_state_dict'])
        G_losses = checkpoint['G_losses']
        D_losses = checkpoint['D_losses']
        iters = checkpoint['iters']

        print("=> loaded checkpoint '{}' (epoch {})"
                  .format(filename, checkpoint['epoch']))
    else:
        print("=> no checkpoint found at '{}'".format(filename))

    return start_epoch, netG, netD, criterion, fixed_noise, optimizerG, optimizerD, G_losses, D_losses, iters

def generate_images(gen, nz, num_images = 100):
    images = []

    with torch.no_grad():
        for _ in range(num_images):
            img = gen(torch.randn(64, nz, 1, 1)).detach().cpu()
            # images.append(vutils.make_grid(img, padding=2, normalize=True))
            images.append(img)

    return images
"""
## Tuneables ##
fresh_start = True

# Root directory for dataset
dataroot = "data/Images"

# Directory for checkpointing model and parameters state
checkpointsroot = "checkpoints/dogs"

# Number of workers for dataloader
workers = 2

# Batch size during training
batch_size = 128

# Spatial size of training images. All images will be resized to this size using a transformer.
image_size = 64

# Number of channels in the training images. For color images this is 3
nc = 3

# Size of z latent vector (i.e. size of generator input)
nz = 100

# Size of feature maps in generator
ngf = 64

# Size of feature maps in discriminator
ndf = 64

# Number of training epochs
num_epochs = 5

# Learning rate for optimizers
lr = 0.0002

# Beta1 hyperparam for Adam optimizers
beta1 = 0.5

# Number of GPUs available. Use 0 for CPU mode.
ngpu = 0
"""

#!/usr/bin/env python3

import os,sys
root = os.path.realpath(os.path.join(os.path.dirname(__file__), "..", ".."))
sys.path.append(root)

from common import *

seed(9999)

def main():
    pass

if __name__ == '__main__':
    main()

## dogganit
We're using generative adversarial networks (GANs) to generate images of dogs. 🐕

### Getting Set Up

This project is designed to run on the [ECE Linux servers](http://www.ece.utexas.edu/it/virtual-linux-resources) (`luigi` specifically) as well as on more traditional Python installations.

>*Note:* Only [one of the scripts](misc/ece-linux/dcgan-II.py) in this project actually is compatible with the ECE Linux servers. We had to make many compromises to actually get things to run on the servers (old version of PyTorch - 0.3.1; before `Tensor`s and `Variable`s were unified) and ultimately the performance benefits were negligible at best. The script (and the requirements file) have been left in for posterity.

1) Install the dependencies. If you're on `luigi`, you'll need to make sure you have a working Python 2.7 installation with `ctypes` working. You'll then probably want to just feed [`requirements.txt`](misc/ece-linux/requirements.txt) into pip:
`pip -r misc/ece-linux/requirements.txt`.

On the other hand, if you're on a machine with a sane python installation, you'll probably want to use a `virtualenv` or higher level tool like [`pipenv`](https://pipenv.readthedocs.io/en/latest/) or [`poetry`](https://python-poetry.org/).

We currently use `pipenv`: if you've got `pipenv` set up, running `pipenv install` should do the trick.

~~_Unfortunately you'll need Python 3.6 or older. See [this](Pipfile#L19-L24) for more details._~~ This is only true of the one (1) ECE Linux compatible script.

<details><summary>For posterity, here's what the note used to say:</summary>
<p>

```toml
# Note: We're using an ancient version of torch because the GLIBC version
# requirements on versions of pytorch newer than 0.3.1 make it not play nice
# with CentOS 6 (luigi).
#
# Because this old version of pytorch predates python 3.7 and 3.8, this also
# forces us to use 3.6.
```

</p>
</details>

2) Grab the data set. In this directory, run:
```bash
mkdir -p data
wget http://vision.stanford.edu/aditya86/ImageNetDogs/images.tar -O - | \
    tar xvf - -C data
```

3) Optionally, grab the checkpoints and stick them in `checkpoints/<name of the model>/`. [Here's one](https://drive.google.com/a/utexas.edu/file/d/1-cIGUu8R90dvXHCtzDjJHNgr5pHN70N-/view?usp=sharing).

The table [below](#the-models) has links for the most current checkpoint for each model.

### The Models

This repository contains several models, each of which has a [Jupyter Notebook](https://jupyter.org/) and a script. The notebooks are for interactive human use and the scripts for batch jobs (i.e. on a server).

Here are the models that we currently have:

| Model Name | Notebook | Script | Description | Last Checkpoint |
|:----------:|:--------:|:------:|-------------|:---------------:|
|   DCGAN 1  | [dcgan-I.ipynb](dcgans/dcgan-I.ipynb)   | [uno.py](dcgans/uno.py) | Baseline model; largely derived from  [here](https://pytorch.org/tutorials/beginner/dcgan_faces_tutorial.html) | [`state_35.pth`](https://drive.google.com/open?id=1-cIGUu8R90dvXHCtzDjJHNgr5pHN70N-) |
|   DCGAN 2  | [dcgan-II.ipynb](dcgans/dcgan-II.ipynb) | [dos.py](dcgans/dos.py) | Like DCGAN 1 but with an extra layer on the generator and the discriminator | [`state_200.pth`](https://drive.google.com/open?id=1EzUuxvhr5iWOTwr0_4ntkSKAlORzmjhW) |
|   DCGAN 3  | [dcgan-III.ipynb](dcgans/dcgan-III.ipynb) | TODO | `image_size` = 128, 60 epochs | [`state_59.pth`](https://drive.google.com/open?id=1ey9T9_ultMoa-mrKTr_fcfYyE20cp7Rw) |
|    Leaky   | [leaky.ipynb](dcgans/leaky.ipynb) | TODO | `image_size` = 128, `batch_size` = 32, `ndf` = 16 w/`ngf` = 64, extra layer in gen and dis for the larger images, LeakyReLU in gen, 155 epochs | [`state_155.pth`](https://drive.google.com/open?id=1iqwyPw5WMr1FwAbUWl5K0p7m2MwIG8pD) |
|   Correct  | [correct.ipynb](dcgans/correct.ipynb) | TODO | `batch_size` = 256, 120 epochs | [`state_119.pth`](https://drive.google.com/open?id=1YxPd7pqIgCe1m1ePUxdOVqdguhpW6ozO) |
|   DCGAN 4  | [dcgan-IV.ipynb](dcgans/dcgan-IV.ipynb) | TODO | `LeakyReLU` used everywhere but with no extra layer | [`state_400.pth`](https://drive.google.com/open?id=1_vhHDxisWuYAMIbrgw46toSKkphxfqKP) |
|  SmoothL1  | [smooth_L1.ipynb](dcgans/smooth_L1.ipynb) | TODO | `SmoothL1Loss` as the loss function, *_(not functional)_* | TODO(@nathan-chin), formerly dogs_smoothL1.ipynb |
| KLDivLoss  | [kl_div_loss.ipynb](dcgans/kl_div_loss.ipynb) | TODO | `KLDivLoss` as the loss function, *_(not functional)_* | TODO(@nathan-chin), formerly dogs_klDivLoss.ipynb |
| BCE Logits | [bce_logits.ipynb](dcgans/bce_logits.ipynb) | TODO | `batch_size` = 32, `BCEWithLogitsLoss` as the loss function, *_(not functional)_* | TODO(@nathan-chin), formerly dogs_bceLogits.ipynb |
|    VAE I   | [vae-I.ipynb](vaes/vae-I.ipynb) | TODO | Variational Autoencoder, *_(not functional)_* | TODO(@nathan-chin), formerly dogs2.ipynb |
|    Alt   | [alt.ipynb](dcgans/alt.ipynb) |  TODO | TODO(@rishab_chander) | N/A |

And here's how they scored when last run (Fréchet Inception Distance (FID) Score):

| Model Name | FID Score |
|:----------:|:---------:|
|   DCGAN 1  | 271.007105|
|   DCGAN 2  | 264.543046|
|   DCGAN 3  | 273.190066|
|    Leaky   | N/A (image size != 64) |
|   Correct  | 245.582173|
|   DCGAN 4  | 322.323548|
|  SmoothL1  |    N/A    |
| KLDivLoss  |    N/A    |
| BCE Logits |    N/A    |
|    VAE I   |    N/A    |
|    Alt     |    N/A    |

### Running the Models

##### Notebooks
Nothing special needs to be done to run the notebooks. If you're using `pipenv` this should do it:
```bash
pipenv shell
jupyter notebook
```

If you're not using `pipenv`, make sure your dependencies are installed and any virtual environments that you're using to contain them are activated and then launch the notebook server (`jupyter notebook`).

##### Scripts
These are a little more complicated. By default (when given no arguments) these look for a checkpoint for the model and then proceed to score the model from the checkpoint. If no checkpoint can be found, the script trains the given model from scratch and then scores the model.

In scoring the model, the script will generate images which it leaves in the generated folder (by default this is just called `generated` and lives at the root of the repo).

All the scripts can simply be executed like programs: `./dcgans/uno.py`. Running from the project root or any of the subfolders should work; the scripts should behave either way (i.e. `./uno.py` with pwd = `<root>/dcgans` should work fine).

<details><summary>ECE Linux Scripts</summary>
<p>

*For the [one ECE Linux Script](misc/ece-linux/dcgan-II.py)*: If you're *not* running on `luigi`, take care to pass the scripts into your python interpreter (i.e. `python3.6 dcgans/uno.py`) instead of simply executing the scripts (i.e. `./dcgans/uno.py`). The [interpreter directive](https://en.wikipedia.org/wiki/Shebang_(Unix)) for the scripts is fixed to the path of the custom Python 2.7 installation on `luigi` for convenience.

</p>
</details>

(TODO(@rrbutani): document the flags if they end up existing)
